<!DOCTYPE html>
<html lang="es">

<head>
    <?php
    include_once("empleado.class.php");
    ?>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1">
    <title>Datos de los empleados</title>
    <link rel="stylesheet" href="stilos.css">
</head>

<body>
    <?php

    if (isset($_POST['enviar'])) {
        if (isset($_POST['enviar'])) {
            echo "<h4>Boleta de pago del empleado</h4>";
            $name = (isset($_POST['nombre'])) ? $_POST['nombre'] :
                "";
            $apellido = (isset($_POST['apellido'])) ?
                $_POST['apellido'] : "";
            $sueldo = (isset($_POST['sueldo'])) ?
                doubleval($_POST['sueldo']) : 0.0;
            $numHorasExtras = (isset($_POST['horasextras'])) ?
                intval($_POST['horasextras']) : 0;
            $pagohoraextra = (isset($_POST['pagohoraextra'])) ?
                floatval($_POST['pagohoraextra']) : 0.0;
            $empleado1 = new Empleados();
            $empleado1->obtenerSalarioNeto($name, $apellido, $sueldo, $numHorasExtras, $pagohoraextra);
        }
    } else {
    ?>
        <section>
            <h1>Formulario empleado</h1>

            <div class="anuncio">
                <div class="fw-300 centrar-texto">
                    <article>
                        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
                            <fieldset>
                                <div>
                                    <label for="nombre">Nombre empleado:</label><br>
                                    <input type="text" name="nombre" id="nombre" size="30" maxlength="35" class="form-control" /><br/>
                                </div>
                                <div class="form-group">
                                    <label for="apellido">Apellido empleado:</label><br>
                                    <input type="text" name="apellido" id="apellido" size="30" maxlength="35"class="form-control" /><br />
                                </div>
                                <div class="form-group">
                                    <label for="sueldo">Sueldo del empleado $:</label><br>
                                    <input type="text" name="sueldo" id="sueldo" size="8" maxlength="8"class="form-control" /><br />
                                </div>
                                <div class="form-group">
                                    <label for="horasextras">Numero horas extras:</label><br>
                                    <input type="text" name="horasextras" id="horasextras" size="4" maxlength="2"class="form-control" /><br />
                                </div>
                                <div class="form-group">
                                    <label for="pogohoraextra">Pago por hora extra:</label><br>
                                    <input type="text" name="pagohoraextra" id="pagohoraextra" size="4" maxlength="6" class="form-control" /><br />
                                </div>
                                <input class="boton boton-verde" type="submit" name="enviar" value="Enviar">&nbsp;
                                <input class="boton boton-verde" type="reset" name="limpiar" value="Restablecer">
                            </fieldset>
                        </form>
                </div>
            </div>
        <?php
    }
        ?>
        </article>
        </section>
</body>

</html>