<?php

class Empleados
{

    private $nombre = "";
    private $apellido = "";
    private $sueldoNominal;
    private $sueldoLiquido;
    private static $idEmpleado = 0;
    private $pago_horas_extra;
    private $isss;
    private $renta;
    private $afp;

    const descuento_AFP = 0.0625;
    const descuento_ISSS = 0.03;
    const descuento_RENTA = 0.075;

    function __construct()
    {
        self::$idEmpleado++;
        $this->nombre = "";
        $this->apellido = "";
        $this->sueldoLiquido = 0.0;
        $this->pago_horas_extra = 0.0;
    }
    function __destruct()
    {
        $backlink = "<a href=\"sueldoneto.php\">";
        $backlink .= "<span>Calcular salario</span>";
        $backlink .= "<span> a otro empleado</span>";
        $backlink .= "<span></span>";
        $backlink .= "</a>";
        echo $backlink;
    }
    function obtenerSalarioNeto(
        $nombre,
        $apellido,
        $salario,
        $horasextras,
        $pago_horas_extra = 0.0
    ) {
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->pago_horas_extra = $horasextras * $pago_horas_extra;
        $this->sueldoNominal = $salario;
        if ($this->pago_horas_extra > 0) {
            $this->isss = ($salario + $this->pago_horas_extra) * self::descuento_ISSS;
            $this->afp = ($salario + $this->pago_horas_extra) * self::descuento_AFP;
        } else {
            $this->isss = $salario * self::descuento_ISSS;
            $this->afp = $salario * self::descuento_AFP;
        }

        $salariocondescuento = $this->sueldoNominal - ($this->isss + $this->afp);
        if ($salariocondescuento > 2038.10) {
            $this->renta = $salariocondescuento * 0.3;
        } elseif (
            $salariocondescuento > 895.24 && $salariocondescuento <= 2038.10
        ) {
            $this->renta = $salariocondescuento * 0.2;
        } elseif (
            $salariocondescuento > 472.00 && $salariocondescuento <= 895.24
        ) {
            $this->renta = $salariocondescuento * 0.1;
        } elseif (
            $salariocondescuento > 0 && $salariocondescuento <= 472.00
        ) {
            $this->renta = 0.0;
        }
        $this->sueldoNominal = $salario;
        $this->sueldoLiquido = $this->sueldoNominal + $this->pago_horas_extra - ($this->isss + $this->afp + $this->renta);
        $this->imprimirBoletaPago();
    }
    function imprimirBoletaPago()
    {
        echo "<!DOCTYPE html>
        <html lang='es'>
        <head>
            <meta charset='UTF-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1.0'>
            <link rel='stylesheet' href='stilos.css'>
        </head>";
        echo "<body>";
        $tabla =  "<div class='anuncio'>";
        $tabla .= "<div class='fw-300 centrar-texto'>";
        $tabla .= "<table border='1'><tr>";
        $tabla .= "<td>Id empleado: </td>";
        $tabla .= "<td>" . self::$idEmpleado . "</td></tr>";
        $tabla .= "<tr><td>Nombre empleado: </td>\n";
        $tabla .= "<td>" . $this->nombre . " " . $this->apellido . "</td></tr>";
        $tabla .= "<tr><td>Salario nominal: </td>";
        $tabla .= "<td>$ " . number_format($this->sueldoNominal, 2, '.', ',') . "</td></tr>";
        $tabla .= "<tr><td>Salario por horas extras: </td>";
        $tabla .= "<td>$ " . number_format($this->pago_horas_extra, 2, '.', ',') . "</td></tr>";
        $tabla .= "<tr><td colspan='2'><h5>Descuentos</h5></td></tr>";
        $tabla .= "<tr><td>Descuento seguro social: </td>";
        $tabla .= "<td>$ " . number_format($this->isss, 2, '.', ',') . "</td></tr>";
        $tabla .= "<tr><td>Descuento AFP:</td>";
        $tabla .= "<td>$ " . number_format($this->afp, 2, '.', ',') . "</td></tr>";
        $tabla .= "<tr><td>Descuento renta: </td>";
        $tabla .= "<td>$ " . number_format($this->renta, 2, '.', ',') . "</td></tr>";
        $tabla .= "<tr><td>Total descuentos: </td>";
        $tabla .= "<td>$ " . number_format($this->isss + $this->afp + $this->renta, 2, '.', ',') . "</td></tr>";
        $tabla .= "<tr><td>Sueldo líquido a pagar: </td>";
        $tabla .= "<td> $" . number_format($this->sueldoLiquido, 2, '.', ',') . "</td></tr>";
        $tabla .= "</table>";
        $tabla .= "</div>";
        $tabla .= "</div>";
        echo $tabla;
        echo " </body>";
        echo "</html>";
    }
}
